import ssl
import urllib.request
from requests.exceptions import RequestException
from urllib.error import URLError

from scrapy.item import Item, Field
from itemloaders.processors import TakeFirst, MapCompose
from w3lib.html import remove_tags


def get_is_online(url: str) -> bool:
	"""
	Send request to open private server url.
	Use context to guard against ssl errors
	and user agent in headers to guard against blocking python url mod_security.
	See https://stackoverflow.com/questions/16627227/problem-http-error-403-in-python-3-web-scraping
	:return: Whether request was successful
	"""
	ctx = ssl.create_default_context()
	ctx.check_hostname = False
	ctx.verify_mode = ssl.CERT_NONE
	headers = {'User-Agent': 'Mozilla/5.0'}
	request = urllib.request.Request(url, headers=headers)
	try:
		code = urllib.request.urlopen(request).getcode()
		return code == 200
	except (RequestException, ValueError, URLError):
		return False


def parse_votes_from_string(value):
	return value.split(' ')[0]


class PrivateServerItem(Item):
	is_online = Field(input_processor=MapCompose(remove_tags, get_is_online), output_processor=TakeFirst())
	name = Field(input_processor=MapCompose(remove_tags), output_processor=TakeFirst())
	url = Field(input_processor=MapCompose(remove_tags), output_processor=TakeFirst())
	votes = Field(input_processor=MapCompose(remove_tags, parse_votes_from_string), output_processor=TakeFirst())
	level_cap = Field()
	civilizations = Field()
	linux_compatible = Field()
