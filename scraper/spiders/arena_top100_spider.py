import scrapy
from scrapy.loader import ItemLoader

from scraper.items.private_server_item import PrivateServerItem


class ArenaTop100SpiderSpider(scrapy.Spider):
	name = 'arena_top100_spider'
	allowed_domains = ['www.arena-top100.com']
	start_urls = ['https://www.arena-top100.com/silkroad-private-servers/']

	def start_requests(self):
		yield scrapy.Request('https://www.arena-top100.com/silkroad-private-servers/', self.parse)

	def parse(self, response, **kwargs):
		# Skip first element, which is promoted server
		private_servers = response.css('table')[1].css('tbody>tr:not([class$="ad-break"])')[1:]

		for ps in private_servers:
			item = ItemLoader(item=PrivateServerItem(), selector=ps)

			item.add_css('name', 'h2>a::text')
			item.add_css('url', 'td>div>h2>a::attr(href)')
			item.add_css('votes', 'a.btn.btn-primary::text')
			# TODO use regex from info box or add for other sites
			# TODO make is_online async using scrapy requests and responses
			# item.add_css('is_online', 'td>div>h2>a::attr(href)')
			# item.add_css('level_cap', level_cap)
			# item.add_css('civilizations', civilizations)
			# item.add_css('linux_compatible', linux_compatible)

			yield item.load_item()

		next_page_selector = response.css('a.page-link.lh-sm[rel="next"]')
		if next_page_selector:
			next_page = next_page_selector.attrib['href']
			yield response.follow(next_page, callback=self.parse)
