import os
from pathlib import Path
from unittest import TestCase

from scrapy.http import TextResponse, Request

from scraper.spiders.arena_top100_spider import ArenaTop100SpiderSpider


class TestArenaTop100SpiderSpider(TestCase):
	file_path = Path(os.path.dirname(__file__)) / "fixtures" / "arena_top100.html"

	@staticmethod
	def fake_response_from_file(file_name, url=None):
		"""
		Create a Scrapy fake HTTP response from an HTML file
		@param file_name: The relative filename from the fixture directory, but absolute paths are also accepted.
		@param url: The URL of the response.
		returns: A scrapy HTTP response which can be used for unit testing.
		"""
		if not url:
			url = 'https://www.arena-top100.com/silkroad-private-servers/'

		request = Request(url=url)
		if not file_name[0] == '/':
			responses_dir = TestArenaTop100SpiderSpider.file_path
			file_path = os.path.join(responses_dir, file_name)
		else:
			file_path = file_name

		with open(file_path, 'r') as fh:
			file_content = fh.read()

			response = TextResponse(
				url=url,
				request=request,
				body=file_content,
				encoding='utf-8'
			)
			return response

	def setUp(self) -> None:
		self.spider = ArenaTop100SpiderSpider()

	def test_start_requests(self):
		expected_count = 25
		results = self.spider.parse(self.fake_response_from_file(str(self.file_path)))
		values = list(results)[:-1]
		self.assertEqual(expected_count, len(values))
		for index, item in enumerate(values):
			with self.subTest(index):
				self.assertIsNotNone(item['name'])
				self.assertIsNotNone(item['url'])
				self.assertIsNotNone(item['votes'])
