FROM debian:bullseye-slim as base

WORKDIR /project

RUN apt-get update -qq \
      && apt-get install -yqq \
           git \
           python3-pip \
      # Make `python` available
      && update-alternatives --install /usr/bin/python python /usr/bin/python3 0 \
      && rm -rf /var/lib/apt/lists/*

COPY requirements*.txt ./
RUN pip install -r requirements.txt

FROM base as ci
RUN pip install -r requirements.dev.txt
